package com.example.davaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        init()
    }


    private fun init(){
        Glide.with(this)
            .load("https://images.unsplash.com/photo-1531804055935-76f44d7c3621?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80")
            .into(cover)
        Glide.with(this)
            .load("https://yt3.ggpht.com/ytc/AAUvwnh6TZF7PRHXPa_Z73do6fkdnqomMxmsEDrNbYcqBw=s900-c-k-c0x00ffffff-no-rj")
            .into(profilepic)
    }
}