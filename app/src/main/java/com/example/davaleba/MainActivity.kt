package com.example.davaleba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        loginbutton.setOnClickListener {
            if (username.text.toString().isNotEmpty() && password.text.toString()
                    .isNotEmpty()
            ){
                activity2()
            }
        }
    }
    private fun activity2(){
        val intent = Intent(this, MainActivity2::class.java)
        startActivity(intent)
    }

}
